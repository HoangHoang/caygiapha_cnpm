USE [master]
GO
/****** Object:  Database [FamilyTree]    Script Date: 14/01/2018 12:25:17 SA ******/
CREATE DATABASE [FamilyTree]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FamilyTree', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FamilyTree.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'FamilyTree_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.SQLEXPRESS\MSSQL\DATA\FamilyTree_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [FamilyTree] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FamilyTree].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FamilyTree] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FamilyTree] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FamilyTree] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FamilyTree] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FamilyTree] SET ARITHABORT OFF 
GO
ALTER DATABASE [FamilyTree] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [FamilyTree] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [FamilyTree] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FamilyTree] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FamilyTree] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FamilyTree] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FamilyTree] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FamilyTree] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FamilyTree] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FamilyTree] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FamilyTree] SET  DISABLE_BROKER 
GO
ALTER DATABASE [FamilyTree] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FamilyTree] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FamilyTree] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FamilyTree] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FamilyTree] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FamilyTree] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [FamilyTree] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FamilyTree] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FamilyTree] SET  MULTI_USER 
GO
ALTER DATABASE [FamilyTree] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FamilyTree] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FamilyTree] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FamilyTree] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
USE [FamilyTree]
GO
/****** Object:  Table [dbo].[DiaDiem]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiaDiem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DiaDiem] [nvarchar](50) NULL,
 CONSTRAINT [PK_DiaDiem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GiaPha]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiaPha](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GiaPha] [nvarchar](10) NULL,
 CONSTRAINT [PK_GiaPha] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ketthuc]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ketthuc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ngkt] [date] NULL,
	[thanhvien] [int] NULL,
	[nguyennhan] [int] NULL,
	[diadiem] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiThanhTich]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiThanhTich](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ThanhTich] [nvarchar](50) NULL,
 CONSTRAINT [PK_LoaiThanhTich] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NgheNghiep]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NgheNghiep](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NgheNghiep] [nvarchar](20) NULL,
 CONSTRAINT [PK_NgheNghiep] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[NguyenNhan]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NguyenNhan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[NguyenNhan] [nvarchar](30) NULL,
 CONSTRAINT [PK_NguyenNhan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QuanHe]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuanHe](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QuanHe] [nvarchar](15) NULL,
 CONSTRAINT [PK_QuanHe] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[QueQuan]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QueQuan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[QueQuan] [nvarchar](20) NULL,
 CONSTRAINT [PK_QueQuan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThanhTich]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThanhTich](
	[Thanhvien] [int] NOT NULL,
	[Thanhtich] [int] NOT NULL,
	[ngpSinh] [date] NOT NULL,
 CONSTRAINT [PK_ThanhTich] PRIMARY KEY CLUSTERED 
(
	[Thanhvien] ASC,
	[Thanhtich] ASC,
	[ngpSinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThanhVIen]    Script Date: 14/01/2018 12:25:17 SA ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThanhVIen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ten] [nvarchar](15) NULL,
	[qhe] [int] NULL,
	[childid] [int] NULL,
	[ngGhi] [date] NULL,
	[doi] [int] NULL,
	[GiaPha] [int] NULL,
	[ngSinh] [date] NULL,
	[qquan] [int] NULL,
	[NgeNghiep] [int] NULL,
	[gioitinh] [nvarchar](3) NULL,
 CONSTRAINT [PK_ThanhVIen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[DiaDiem] ON 

INSERT [dbo].[DiaDiem] ([Id], [DiaDiem]) VALUES (1, N'Nghĩa trang Bình Hưng Hòa')
INSERT [dbo].[DiaDiem] ([Id], [DiaDiem]) VALUES (2, N'Nghĩa trang Gò Dưa')
INSERT [dbo].[DiaDiem] ([Id], [DiaDiem]) VALUES (3, N'Nghĩa trang & Chùa Nghệ Sĩ')
SET IDENTITY_INSERT [dbo].[DiaDiem] OFF
SET IDENTITY_INSERT [dbo].[ketthuc] ON 

INSERT [dbo].[ketthuc] ([id], [ngkt], [thanhvien], [nguyennhan], [diadiem]) VALUES (1, CAST(N'2011-10-21' AS Date), 3, 1, 1)
INSERT [dbo].[ketthuc] ([id], [ngkt], [thanhvien], [nguyennhan], [diadiem]) VALUES (2, CAST(N'2012-10-22' AS Date), 4, 1, 2)
SET IDENTITY_INSERT [dbo].[ketthuc] OFF
SET IDENTITY_INSERT [dbo].[LoaiThanhTich] ON 

INSERT [dbo].[LoaiThanhTich] ([Id], [ThanhTich]) VALUES (1, N'Thực hiện nghiêm chỉnh kế hoạch hoá gia đình')
INSERT [dbo].[LoaiThanhTich] ([Id], [ThanhTich]) VALUES (2, N'Tích cực công tác từ thiện, đến ơn đáp nghĩa')
INSERT [dbo].[LoaiThanhTich] ([Id], [ThanhTich]) VALUES (3, N'Xây dụng gia đình văn hóa, hòa thuận, hạnh phúc')
INSERT [dbo].[LoaiThanhTich] ([Id], [ThanhTich]) VALUES (4, N'Bằng khen học sinh giỏi cấp Tỉnh')
INSERT [dbo].[LoaiThanhTich] ([Id], [ThanhTich]) VALUES (5, N'Bằng khen học sinh giỏi cấp Huyện')
SET IDENTITY_INSERT [dbo].[LoaiThanhTich] OFF
SET IDENTITY_INSERT [dbo].[NgheNghiep] ON 

INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (1, N'Giáo viên ')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (2, N'Kinh doanh')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (4, N'Công chức viên')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (5, N'Luật sư')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (6, N'Kế toán')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (7, N'Gia công nội thất')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (8, N'Thợ mộc')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (9, N'Bếp trưởng')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (10, N'Bác sĩ')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (11, N'Học - Sinh viên')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (12, N'Thợ điện')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (13, N'Thợ may')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (14, N'Nhân viên IT')
INSERT [dbo].[NgheNghiep] ([Id], [NgheNghiep]) VALUES (15, N'Điện lạnh')
SET IDENTITY_INSERT [dbo].[NgheNghiep] OFF
SET IDENTITY_INSERT [dbo].[NguyenNhan] ON 

INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (1, N'Tai nạn xe')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (2, N'Bệnh tim')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (3, N'Ung thư phổi')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (4, N'Đột quỵ')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (5, N'Huyết áp cao')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (6, N'Tiểu đường')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (7, N'Bệnh lý về thận')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (8, N'Tiêu chảy')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (9, N'Ung thư gan')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (10, N'Rối loạn nhịp tim')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (11, N'Thuyên tắc phổi')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (13, N'Bóc tách động mạch chủ')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (14, N'Stress tâm lý')
INSERT [dbo].[NguyenNhan] ([Id], [NguyenNhan]) VALUES (15, N'Sử dụng chất kích thích')
SET IDENTITY_INSERT [dbo].[NguyenNhan] OFF
SET IDENTITY_INSERT [dbo].[QuanHe] ON 

INSERT [dbo].[QuanHe] ([Id], [QuanHe]) VALUES (1, N'Con')
INSERT [dbo].[QuanHe] ([Id], [QuanHe]) VALUES (2, N'Vo,Chong')
SET IDENTITY_INSERT [dbo].[QuanHe] OFF
SET IDENTITY_INSERT [dbo].[QueQuan] ON 

INSERT [dbo].[QueQuan] ([Id], [QueQuan]) VALUES (1, N'TP.HCM')
INSERT [dbo].[QueQuan] ([Id], [QueQuan]) VALUES (2, N'Đồng Nai')
INSERT [dbo].[QueQuan] ([Id], [QueQuan]) VALUES (3, N'Biên Hòa')
INSERT [dbo].[QueQuan] ([Id], [QueQuan]) VALUES (4, N'Trạng Boom')
SET IDENTITY_INSERT [dbo].[QueQuan] OFF
INSERT [dbo].[ThanhTich] ([Thanhvien], [Thanhtich], [ngpSinh]) VALUES (1, 1, CAST(N'2017-10-21' AS Date))
INSERT [dbo].[ThanhTich] ([Thanhvien], [Thanhtich], [ngpSinh]) VALUES (1, 1, CAST(N'2018-01-20' AS Date))
INSERT [dbo].[ThanhTich] ([Thanhvien], [Thanhtich], [ngpSinh]) VALUES (2, 3, CAST(N'2017-02-21' AS Date))
INSERT [dbo].[ThanhTich] ([Thanhvien], [Thanhtich], [ngpSinh]) VALUES (3, 5, CAST(N'2018-01-01' AS Date))
SET IDENTITY_INSERT [dbo].[ThanhVIen] ON 

INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (1, N'Nguyễn Văn O', NULL, NULL, CAST(N'1967-10-21' AS Date), 1, NULL, CAST(N'1967-10-21' AS Date), 1, 2, N'Nam')
INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (2, N'Trần Thị A', 2, 1, CAST(N'1990-10-22' AS Date), 1, NULL, CAST(N'1968-01-21' AS Date), 3, 2, N'Nữ')
INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (3, N'Nguyễn Văn A', 1, 1, CAST(N'2011-10-21' AS Date), 2, NULL, CAST(N'2011-10-22' AS Date), 1, 11, N'Nam')
INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (4, N'Nguyễn Văn B', 1, 1, CAST(N'2012-10-23' AS Date), 2, NULL, CAST(N'2012-10-24' AS Date), 1, 11, N'Nam')
INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (5, N'Nguyễn Văn C', 1, 1, CAST(N'2011-10-24' AS Date), 2, NULL, CAST(N'2011-10-25' AS Date), 1, 11, N'Nam')
INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (8, N'Nguyễn Văn D', 1, 1, CAST(N'2011-10-23' AS Date), 2, NULL, CAST(N'2011-10-24' AS Date), 1, 11, N'Nam')
INSERT [dbo].[ThanhVIen] ([Id], [ten], [qhe], [childid], [ngGhi], [doi], [GiaPha], [ngSinh], [qquan], [NgeNghiep], [gioitinh]) VALUES (9, N'Nguyễn Văn D', 1, 1, CAST(N'2012-11-11' AS Date), 2, NULL, CAST(N'2012-11-12' AS Date), 1, 11, N'Nam')
SET IDENTITY_INSERT [dbo].[ThanhVIen] OFF
ALTER TABLE [dbo].[ketthuc]  WITH CHECK ADD  CONSTRAINT [FK_ketthuc_DiaDiem] FOREIGN KEY([diadiem])
REFERENCES [dbo].[DiaDiem] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ketthuc] CHECK CONSTRAINT [FK_ketthuc_DiaDiem]
GO
ALTER TABLE [dbo].[ketthuc]  WITH CHECK ADD  CONSTRAINT [FK_ketthuc_NguyenNhan] FOREIGN KEY([nguyennhan])
REFERENCES [dbo].[NguyenNhan] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ketthuc] CHECK CONSTRAINT [FK_ketthuc_NguyenNhan]
GO
ALTER TABLE [dbo].[ketthuc]  WITH CHECK ADD  CONSTRAINT [FK_ketthuc_ThanhVIen] FOREIGN KEY([thanhvien])
REFERENCES [dbo].[ThanhVIen] ([Id])
GO
ALTER TABLE [dbo].[ketthuc] CHECK CONSTRAINT [FK_ketthuc_ThanhVIen]
GO
ALTER TABLE [dbo].[ThanhTich]  WITH CHECK ADD FOREIGN KEY([Thanhvien])
REFERENCES [dbo].[ThanhVIen] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ThanhTich]  WITH CHECK ADD  CONSTRAINT [FK__ThanhTich__Thanh__628FA481] FOREIGN KEY([Thanhtich])
REFERENCES [dbo].[LoaiThanhTich] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ThanhTich] CHECK CONSTRAINT [FK__ThanhTich__Thanh__628FA481]
GO
ALTER TABLE [dbo].[ThanhVIen]  WITH CHECK ADD  CONSTRAINT [FK_ThanhVIen_GiaPha] FOREIGN KEY([GiaPha])
REFERENCES [dbo].[GiaPha] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ThanhVIen] CHECK CONSTRAINT [FK_ThanhVIen_GiaPha]
GO
ALTER TABLE [dbo].[ThanhVIen]  WITH CHECK ADD  CONSTRAINT [FK_thanhvien_nghenghiep] FOREIGN KEY([NgeNghiep])
REFERENCES [dbo].[NgheNghiep] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ThanhVIen] CHECK CONSTRAINT [FK_thanhvien_nghenghiep]
GO
ALTER TABLE [dbo].[ThanhVIen]  WITH CHECK ADD  CONSTRAINT [FK_ThanhVIen_QuanHe] FOREIGN KEY([qhe])
REFERENCES [dbo].[QuanHe] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ThanhVIen] CHECK CONSTRAINT [FK_ThanhVIen_QuanHe]
GO
ALTER TABLE [dbo].[ThanhVIen]  WITH CHECK ADD  CONSTRAINT [FK_ThanhVIen_QueQuan] FOREIGN KEY([qquan])
REFERENCES [dbo].[QueQuan] ([Id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[ThanhVIen] CHECK CONSTRAINT [FK_ThanhVIen_QueQuan]
GO
ALTER TABLE [dbo].[ThanhVIen]  WITH CHECK ADD CHECK  (([gioitinh]='Nam' OR [gioitinh]=N'Nữ'))
GO
USE [master]
GO
ALTER DATABASE [FamilyTree] SET  READ_WRITE 
GO
